Tank mod (from War Mod extended) authored by Larcelo.
For use: spawn object tank_spawner from roomscript {type:"tank_spawner", x:0, y:0}
Tanks will appear in random places on map when there are 4 or more wurms in the game.