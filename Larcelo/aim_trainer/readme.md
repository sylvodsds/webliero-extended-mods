Aim training (only for Liero Extended! - link how to install: bit.ly/lieroX) 
This is a mode that allows you to train your accuracy with the most popular weapons such as Rifle, Gauss Gun and Missile. 
Three gameplay modes (activated by using the once Aim Training weapon): 

- static mode (shooting at stationary targets) 
- dynamic mode (targets move left-right, up-down) 
- both static & dynamic at the same time
- Adventure mode (targets attack the player, five levels each lasting 1 minute, and a surprise at the end!)  4 difficulty levels (easy, normal, hard, impossible)

## v5.3

## v5.2

- buff rifle
- hard mode bugs slightly slower (5%)
