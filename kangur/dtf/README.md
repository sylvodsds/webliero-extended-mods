# Wurmz of Tech & Magic (a.k.a. DTF) Mod

a Webliero mod created specifically to play WL extended game modes, in particular defend the flag & capture the flag.

Originally it contained regular weapons and other "elements" (which were not available for players to use but could be spawned on the map using room scripts, e.g. lava, water, lasers, portals etc). In the current version, all those elements were removed from the "main" json file and put into separate "small" json files, which would be merged wih the main one using room script.

Some weapons are my own original ideas but some were inspired by or taken from many other mods created by different ppl, so big thanks to all those fantastic modders and game devs! (Daro, Radon, Kami, Jerac, foobaz, Larcelo and more).

Big thanks to worm Ophi for his extended hacks (which allowed me to make many epic and cool weapons that won't work in non-extended webliero) and to worms dsds & Larcelo for their contribution, ideas and help with making some weapons. Cheers!

### current version: 35e

## WEAPONS WITH BEHAVIORS:

- **KEVLAR MISSILE (object starts shooting bullets towards its direction when enemy worm is close enough)**
- **VENOM RIFLE (bullet sticks to the worm and gives it damage during some time when it moves)**
- **FORCE FIELD (object spawns 2 different types of nObjects with different teamImmunity set)**
- **HELICOPTER (object is destroyable + displays its own hp bar + shoots bullets towards its direction when the owner is inside it)**
- _SUICIDE VEST (object sticks to the owner & is destroyable by other objects & can be detonated remotely)_
- _MAGIC ARMOR (fast regeneration of the owner for some time)_
- _SILENT KILLER (object sticks to the worm and explodes after 3 seconds)_
- _BERSERK (object sticks to the owner and fires bullets towards closest worm)_
- _KILLER DRONE (object follows its owner and fires bullets towards closest enemy)_
- **FAKE FLAG (object sticks only to enemy worms and explodes after some time)**
- **SACRIFICE (object explodes only if enemy worm is close enough & kills the owner but gives +100 hp bonus to its teammates)**
- **FLOAT MINE (object can be pushed by almost all other objects)**
- **X RAY (object is killed when it reaches level bounds)**
- _HELL RITUAL (object gives hp bonus to the owner when he kills other worms with it; otherwise the owner loses hp slowly)_
- _DEATH AURA (object orbits its owner and gives damage to every enemy worm if it is close enough)_
- _INVULNERABILITY (object makes the owner immune for almost all weapons during some time)_
- _DIRT GRINDER (object gives the owner ability to remove all types of dirt on touch)_
- **REGENERATION (object gives some health to the owner when used)**
- **VAMPIRE BITE (object gives damage only to enemy worms & the owner is given +35 hp bonus on enemy worm hit)**
- **DIRT TRAP (object explodes only when enemy worm touches it)**
- ~**PROXY MINE (object attacks closest enemy & is destroyable by other objects)**~
- **ORB LAUNCHER (set current startFrame for the object)**
- **ENERGY DISC (object gives damage only to enemy worms on close distance)**
- **DISARMABLE BOMB (object can be removed without explosion if any worm stands close to it)**
- **MAGNETIC FIELD (object bounces other objects off when used)**
- **ROCKET LAUNCHER (object gives damage only to enemy worms)**
- **REMOTE BOMB (create another object placed on the owner when enemy worm is close to the bomb)**
- _BEHOLDER (create stationary object which fires bullets towards closest enemy if the enemy is close enough)_
- _ABSORBING VORTEX (object removes almost all other objects within its range without explosion when used)_
- _HOMING MISSILE (object attacks closest enemy)_
- _BAYRAKTAR (object shoots bullets towards the closest enemy + is destroyable by other objects)_
- _STARGATE (the owner is teleported to the object's position after pressing detonator key)_
- **MANIPULATOR (object can immobilise and/or relocate almost all other objects within its range when used)**
- **PHASER RIFLE (object kills the worm and turns its gibs into a dirt-made statue)**
- **GRAVITY MINE (object sticks to enemy worm and increases its gravity for some time)**
- **TOXIC SMOKE (object makes enemy worms unable to shoot within its range when used)**
- **FREEZING BEAM (object makes the hit worm unable to move for a short time within its range)**
- _LOOTBOX (object gives randomly one special ability to the worm when touched)_
- _PANDORA BOX (object randomly kills other worm or heals it when touched + does the opposite to the owner)_
- _ADRENALINE RUSH (slow regeneration of the owner while it is in a motion; otherwise it loses hp slowly)_
- _ION CANNON (create powerful beam of energy when enemy worm is within its range)_
- _TRANSLOCATOR (the owner is teleported to the object's position after pressing detonator key)_
- _SPEED UP (object sticks to the owner and speeds it up for some time)_
- **UZUMAKI (object is animated also on rock/dirt/ground collision)**
- **FLAMETHROWER (create another object which sticks to the worm)**
- **PORTAL GUN (teleport the owner instantly on a short fixed distance when used)**
- **SAFETY ZONE (object removes almost all other objects within its range without explosion)**

**bold** - avialable in the mod like all other "non-behavior" weapons

_italics_ - special super-op weapons available only on some maps

# CHANGELOG

### TO-DO & IDEAS

* ~add shooting bombs to enemy worms in HELICOPTER (?)~
* add custom sprites for some gun barrels (?)
* ~add some weapons from Magic Playground and/or csliero (?)~
* create some brand new weapons (?)
* add some vehicles with behaviors (?)
* nerf REMOTE BOMB (remote detonation only after ground collision) (?)
* add more "super op" weapons
* improve the balance
* remove unnecessary sprites from wlsprt file

### 35e

* change ORB LAUNCHER (shotType: 3)
* improve BOUNCING BOMB a little bit
* miscellaneous fixes & changes

### 35d

* nerf DESOLATOR, SPIRIT BALL and APOCALYPSE RUNE (longer loadingTime)
* nerf VENOM RIFLE, UZUMAKI and ROCKET LAUNCHER (longer loadingTime)
* nerf REMOTE BOMB (less damage on explosion)
* nerf VAMPIRE BITE (2x less ammo)
* nerf LASER PISTOL a bit (longer loadingTime)
* buff DIGGER (shorter loadingTime)
* buff JETPACK (more ammo + shorter loadingTime)
* buff ECTOPLASM BLOB a bit (less delay between shots + more damage)
* buff SUPER SHOTGUN a bit (shorter loadingTime)
* ~remodel SUPER SHOTGUN (faster bullets & shorter reload time but lower range)~
* rename APSHOCKCANNON into AUTOCANNON (dsds likes it)
* remove behaviors from PROXY MINE and ORB LAUNCHER (sic!)
* buff "healing gems" (longer lifetime + doubled hp bonus)
* reduce the plausibility of creating "WEAPONS" crate & healing gems on worm's death
* miscellaneous fixes & changes

### 35c (experimental)

* ~add behavior to SMOKE GRENADE (create another object which sticks to worms)~
* buff TOXIC SMOKE a bit (shorter reload time + longer timeToExplo + higher range)
* add small healing effect in SAFETY ZONE and PROTECTION
* add behavior to X RAY (kill the object when it reaches level bounds)
* add new weapon: ECTOPLASM BLOB (based on TOXIC BLOB from Magic Playground)
* create special "weapon replacement crate" and healing gems with onPlayerDeath
* change loading time in PROTECTION, BARRIER, MANIPULATOR and SAFETY ZONE
* miscellaneous changes & improvements

### 35b

* change FLOAT MINE (shorter timeToExplo + behavior added)
* rebalancing some weapons (DESOLATOR, ROCKET LAUNCHER and UZI)
* miscellaneous changes & improvements

### 35a (experimental)

* improve PORTAL GUN behavior with api.getMaterial method
* change VENOM RIFLE a bit (different marker + dmg is given only when the target moves)
* change HELICOPTER a bit (shoot bullets when the owner is inside)
* miscellaneous fixes & changes

### 35

* add 7 new weapons (including 1 with behavior)
* miscellaneous fixes & changes

### 34d

* improve PORTAL GUN marker again (thx dsds!)
* revert some changes implemented in previous version
* miscellaneous fixes & changes

### 34c (experimental)

* improve PORTAL GUN marker (associate with owner.aimAngle)
* remodel & buff SHOCKWAVE BOMB (more speed, gravity, detectDistance and timeToExplo)
* miscellaneous changes & improvements

### 34b

* buff CHIQUITA GUN and TORPEDO TUBE a little bit (+1 hitDamage)
* buff EXCIMER LASER a little bit (+2 detectDistance)
* buff FLAK CANNON a little bit (+1 ammo)
* buff SHOCKWAVE BOMB (more powerful central explosion)

### 34a

* change PORTAL GUN (crosshair visible only for the owner + shorter reload time)
* miscellaneous fixes & changes

### 34 (experimental)

* improve behavior in VENOM RIFLE (update + postUpdate)
* improve behavior in DIRT TRAP and ROCKET LAUNCHER (save the owner and its team)
* improve behavior in GRAVITY MINE and PHASER RIFLE (remove unnecessary wObjects)
* improve behavior in FAKE FLAG beacons (remove unnecessary wObject)
* add behavior to FREEZING BEAM (to improve the "freezing effect")
* ~add new static behavior (create specified object onPlayerHit)~
* make UZI bullets visible & remove behavior from it (dsds likes it)
* miscellaneous fixes & improvements

### 33f

* improve PORTAL GUN (add teleportation indicator and solid rock detection)
* add new "super op" weapons: TRANSLOCATOR and SPEED UP (with behavior)
* buff TORPEDO TUBE a little bit (more dmg + less delay)

### 33e (experimental)

* add beacons to FAKE FLAG
* add new weapon: PORTAL GUN (with behavior)
* nerf HELICOPTER a bit (less hp + higher range)
* miscellaneous fixes & improvements

### 33d

* improve & simplify behavior in DISARMABLE BOMB (remove unnecessary wObject)
* nerf ORB LAUNCHER and PROXY MINE (objects don't follow other worms when the owner is dead)
* nerf AIR STRIKE (add some delay time before explosion)
* miscellaneous fixes & changes

### 33c (experimental)

* add platform: true property for HELICOPTER
* add simple behavior to FLAMETHROWER (stickable flames)
* buff TORPEDO TUBE (more ammo + less delay)
* buff UZI a little bit (more dmg + less reload time)
* buff EXCIMER LASER a little bit (less delay)
* buff SMOKE GRENADE a little bit (more dmg)
* miscellaneous fixes & changes

### 33b

* simplify behavior in GRAVITY MINE (remove unnecessary nObject)
* change launchSound for BAZOOKA and KEVLAR MISSILE
* add new weapon: UZI (with simple behavior)
* miscellaneous fixes & changes

### 33a

* improve behavior in PHASER RIFLE - hide reload animation after second shot (thx dsds!)
* improve behavior in PROXY MINE (display "hurt" sprite when hit)
* nerf ORB LAUNCHER a little bit (less damage + more pushback on explosion)
* buff TUPOLEV (+20 hitDamage)
* miscellaneous fixes & changes

### 33

* update behavior in VAMPIRE BITE (thx Ophi)
* add behavior to UZUMAKI (animate on ground)
* improve behavior in PHASER RIFLE (use t.direction instead of t.vx / t.vy)
* add new weapon (without behavior): TORPEDO TUBE (based on KATYUSHA from War Mod)

### 32f

* small buff in SACRIFICE and PIXEL BOMB (higher range of explosion)
* nerf FIRECRACKER a little bit (less damage & range of explosion)
* nerf FIREBALL a little bit (shorter timeToExplo)
* nerf SPIRIT BALL a little bit (less damage)
* buff FLAMETHROWER a little bit (longer timeToExplo)
* improve behavior in TOXIC SMOKE
* fix bug in PROXY MINE behavior (thx dsds)
* miscellaneous fixes & changes
* rename the mod

### 32e

* change behavior in HELICOPTER (add hp bar + less hp)
* replace ACCELERATE with SONIC SLASH (based on BROOK'S SLASH from Daro WA mod)
* some minor fixes & changes

### 32d

* improve behavior in TOXIC SMOKE (thx wgetch & Larcelo)
* small nerf in AIR STRIKE and ELECTRIC FENCE (less damage)
* change behavior in PROXY MINE: make it destroyable (thx dsds)
* nerf UZUMAKI: longer timeToExplo (thx Larcelo & dsds)
* some other minor fixes & changes

### 32c

* re-add FORCE FIELD to standard mod (with better behavior & small nerf)
* improve behavior in DISARMABLE BOMB and VENOM RIFLE
* add behavior to super op weapon ION CANNON
* miscellaneous changes & improvements

### 32b

* small visual changes in DRAGON FLIES and X RAY
* add one new "super op" weapon: ION CANNON (without behavior)
* add some new sounds
* miscellaneous fixes & changes

### 32a

* re-add ELECTRIC FENCE (without behavior)
* improve behavior in FORCE FIELD and ADRENALINE RUSH
* fix some minor bugs

### 32

* remove ELECTRIC FENCE
* move FORCE FIELD to "super op weapons"
* change behavior in VENOM RIFLE a little bit
* nerf PROXY MINE (shorter timeToExplo)
* miscellaneous fixes & changes

### 31f

* re-add AIR STRIKE & buff it a little bit (sorry dsds)
* nerf FLOAT MINE (longer reload time to prevent spam)
* small fix in APOCALYPSE RUNE (immutable true to parttrail)
* improve behavior in INVULNERABILITY

### 31e

* nerf ELECTRIC FENCE (longer reload + shorter timeToExplo)
* add behavior to ELECTRIC FENCE (make it destroyable)
* remove unnecessary (unused) objects & clean the code
* buff KEVLAR MISSILE a little bit (longer timeToExplo)
* nerf ORB LAUNCHER a little bit (less damage in sObject)

### 31d

* nerf DESOLATOR (less dmg) 
* nerf NAPALM (longer reload & less splinters)
* nerf CRACKFIELD (longer reload)
* nerf SPIRIT BALL (less dmg & longer reload)
* nerf HYDROCHLORIC ACID (less amount of particles)
* nerf HELLFIRE ROCKETS (less ammo)
* buff DOOMSDAY (more ammo)
* buff THROW KNIFE (more dmg)
* buff FAKE FLAG (+1 ammo & higher range)
* buff PULSE GUN (more dmg)
* buff SHOCKWAVE BOMB (more blowAway)
* remove AIR STRIKE & DIRTBALL
* add new weapon: ELECTRIC FENCE (based on BARBED WIRE FENCE from War Mod)

### 31c

* change VAMPIRE BITE a little bit (more dmg per hit - 60, less HP bonus on hit - 30)
* nerf PHASER RIFLE a little bit (reduce ammo from 3 to 2)
* change dirtEffects in SMOKE GRENADE (remove "solid smoke" effect)

### 31b

* nerf VAMPIRE BITE (reduce dmg per hit to 50)
* add immutable: true to FREEZING BEAM objects

### 31a

* nerf PHASER RIFLE again (limited ammo per life)
* nerf VAMPIRE BITE (longer reload time)
* nerf SPIRIT BALL (longer reload time)
* nerf CRIPPLE BULLET (longer delay & reload time)
* nerf HELLFIRE ROCKETS (longer reload time)
* nerf NAPALM (longer reload time)
* nerf KEVLAR MISSILE (remove auto aim from shooting bullets)
* nerf FORCE FIELD (set removeOnSObject: true)
* buff AIR STRIKE (napalm splinter on explosion)
* move HOMING MISSILE to "super op" weapons mod

### 31

* add new weapon: TOXIC SMOKE (with behavior)
* add new weapon: AIR STRIKE
* add 2 new sounds to the soundpack
* nerf & change PHASER RIFLE a little bit

### 30v

* improve behavior in HELICOPTER
* remove behavior from FIRE BALL
* improve behaviors in some "super op" weapons

### 30u

* add new sprites for DIGGER
* add experimental behavior to FIRE BALL
* nerf PHASER RIFLE a little bit (longer reload time)
* improve behavior in SACRIFICE (fix "multi selfkill message" glitch)
* improve behavior in MANIPULATOR (thx Ophi)
* some other minor changes in WLSPRT file

### 30t

* add new weapon: GRAVITY MINE (with behavior)
* nerf PHASER RIFLE (less ammo + longer delay) & change the delay bar
* remove the "stun effect" from VENOM RIFLE
* fix "double selfkill bug in predator mode" in SACRIFICE
* update "super op weapons" plugin to v. 2.4
* miscellaneous fixes & changes

### 30s

* re-add FIRECRACKER (but buffed a little bit)
* improve REMOTE BOMB by adding behavior to it
* remove unnecessary api pack/unpack thingy from some behaviors
* add 3 new "super op weapons" (LOOTBOX, PANDORA BOX and ADRENALINE RUSH)
* miscellaneous fixes and changes

### 30r

* remove behavior from BARRIER
* change PHASER RIFLE a little bit (nerf + add charging bar)
* add 2 new sounds to the soundpack
* miscellaneous fixes & changes

### 30q (experimental)

* change behavior in MAGNETIC FIELD
* add experimental behavior to BARRIER
* add new weapon MANIPULATOR (with behavior) for tests
* add new weapon PHASER RIFLE (with behavior) for tests
* add some new sounds to the soundpack
* add two new "super op weapons" (ABSORBING VORTEX and VERMICIDE)
* miscellaneous fixes and changes

### 30p

* fix behaviors in FORCE FIELD, VENOM RIFLE and SACRIFICE (thx dsds)
* nerf FORCE FIELD, VENOM RIFLE and ORB LAUNCHER a little bit

### 30o

* change OBSTACLE a little bit (longer reload time + add startFrame)
* add experimental behavior to SACRIFICE (for wgetch)
* change FORCE FIELD a little bit (object is not removed on owner's death)
* change behavior in VENOM RIFLE (hit worm cannot shoot during poison injection)
* add "proximity" behavior to ORB LAUNCHER
* improve REGENERATION by adding behavior to it
* miscellaneous minor fixes and changes

### 30n

* improve FORCE FIELD by adding behavior to it
* buff SPIKEBALLS, MINI NUKE and TUPOLEV a little bit
* remove unused behaviors

### 30m

* replace BEHOLDER with FORCE FIELD (from csliero)
* clean the code in KEVLAR MISSILE behavior 
* buff DIGGER and JETPACK a little bit
* nerf HYDROCHLORIC ACID a little bit
* rebalance some other weapons
* change 2 colours in the palette (141 and 142)

### 30l

* remove FIRECRACKER
* improve behavior in SACRIFICE (wgetch likes it)
* improve behavior in DIRT TRAP a little bit
* add one new weapon HOMING MISSILE (based on KE GO from War Mod)
* add one new weapon ROCKET LAUNCHER (based on PARIS CANNON from War Mod)
* add one new weapon BEHOLDER (based on WATCHTOWER from War Mod)
* replace some sprites in wlsprt file
* miscellaneous fixes & changes
* add one new sound to the soundpack

### 30k

* improve MAGNETIC FIELD a little bit
* buff ENERGY DISC a little bit
* change some sprites in wlsprt file

### 30j

* change MAGNETIC FIELD and add behavior to it
* small fixes in ENERGY DISC, FIRE BALL and DISARMABLE BOMB
* replace RADIOACTIVE CLOUD with SMOKE GRENADE (from War Mod)
* change some sprites in wlsprt file
* miscellaneous fixes and changes

### 30i

* improve DISARMABLE BOMB by adding behavior
* improve behavior in FAKE FLAG (thx dsds)
* improve VAMPIRE BITE (fix "no kill but gain hp" bug)
* fix DIRT TRAP
* add new palette & text sprites

### 30h

* add behavior to ENERGY DISC & change it to reduce lags / FPS drop
* improve behavior in DIRT TRAP a little bit

### 30g

* change MAGNETIC FIELD, ENERGY DISC and CRACKFIELD a little bit to reduce lags / FPS drop
* improve VAMPIRE BITE a little bit
* change BUMP sound in the soundpack

### 30f

* remove some "super op weapons" into separate mod
* add "immutable: true" to some objects in HELICOPTER

### 30e

* nerf FORTIFY a little bit (longer reload time)
* buff DIRTBALL (dirt recreates itself automatically for some time)
* nerf KEVLAR MISSILE (shorter TimeToExplo of the main bullet + less dmg per shots)
* buff BOUNCY MINES a little bit (faster bullets and slightly shorter reload time)
* buff DIGGER (shorter reload time)
* buff JETPACK a little bit (+10 ammo)
* nerf SPIRIT BALL a little bit (slighlty longer reload time)
* clean the code (remove unnecessary "startFrame: 79" cases)

### 30d

* fix behavior in PROXY MINE (thx dsds)
* fix behavior in SATELLITE (thx Ophi)
* nerf PROXY MINE a little bit (more reload time + slower "hunting" speed)
* nerf FLOAT MINE a little bit (reduce parts from 3 to 2 per shot)
* small change in behavior in DIRT TRAP

### 30c

* change appearance of MAGIC ARMOR (add "duration bar" displayed above the worm)
* improve DIRT TRAP by adding behavior to it
* improve PROXY MINE by adding behavior to it
* miscellaneous fixes + clean the code

### 30b

* improve VAMPIRE BITE by adding behavior to it and remove unnecessary nObjects (fix "selfkill" bug)
* rename UZI into CARBINE and buff it a little bit (less distribution of bullets)
* improve DISARMABLE BOMB "stage3" (easier to defuse it - more detectDistance in nid67)
* buff FAKE FLAG a little bit (splinterAmount +1)
* miscellaneous fixes & improvements

### 30a

* unlock two new weapons: HELL RITUAL and SATELLITE (with fixed behaviors)
* miscellaneous fixes & changes

### 29h

* fix KILLER DRONE behavior (it does not shoot towards owner - thanks Larcelo again)
* add behavior to SACRIFICE (now it heals only team mates)
* change launchSound for BERSERK
* rename VEHICLE into HELICOPTER
* miscellaneous fixes & changes

### 29g

* remove behavior from ORB LAUNCHER
* change appearance of KILLER DRONE (new sprites used)
* add new weapon: FAKE FLAG (with fixed behavior - thx Larcelo)
* change launchSound for some weapons

### 29f

* change VENOM RIFLE behavior (less damage + add "marker" above the hit worm)
* add new behavior: fake flag (currently unused)
* unlock one hidden weapon: KILLER DRONE (for testing)

### 29e

* add behavior to ORB LAUNCHER for testing (+ small change)
* fix bug in SATELLITE (add missing owner)
* change appearance + behavior of KILLER DRONE (shoots laser pistol again with some cool effects)

### 29d

* change behavior of BERSERK (throws uzumaki instead of grenades)
* fix bug in BERSERK behavior (thx Larcelo)
* fix bug in KILLER DRONE behavior (add missing owner)
* change appearance + behavior of KILLER DRONE (shoots x ray instead of laser pistol)
* small change in behavior of SATELLITE

### 29c

* add more "life" to SUICIDE VEST (from 10 to 80)
* add one new weapon: BERSERK (with behavior)
* add two hidden (unused) weapons: SATELLITE and HELL RITUAL (with behaviors)

### 29b

* nerf MAGIC ARMOR (reduce timeToExplo from 1200 frames to 600 frames)
* nerf SILENT KILLER (limited ammo per life: 3)
* rename SATELLITE DRONE into KILLER DRONE
* miscellaneous fixes & changes

### 29a

* re-added original behavior to KEVLAR MISSLE for testing
* restore DARTGUN to previous state (with some small changes) + remove behavior from it
* rename RIFLE into VENOM RIFLE (+ add bevavior to it)
* remove remote detonation from SILENT KILLER
* add new weapon: MINIGUN
* clean the code (remove some unused objects)
* add one hidden (unused) weapon: SATELLITE DRONE (with behavior)

### 28h

* re-added behavior to VEHICLE for testing
* bunch of fixes to behavior which potentially could add issues

### 28g

* remove behavior from VEHICLE
* change behavior in KEVLAR MISSILE

### 28f

* fix DARTGUN

### 28e

* rename IMMORTALITY to MAGIC ARMOR and change its behavior

### 28d

* rename LIGHTNING FLASH into FLASH SPARKS
* fix LIGHTNING GUN (change blowAway in wObject)
* buff VEHICLE (more timeToExplo)
* buff SILENT KILLER (more timeToExplo)
* buff IMMORTALITY (more timeToExplo)
* buff SUICIDE VEST (more timeToExplo)

### 28c

* remove INVULNERABILITY
* changes in SILENT KILLER
* changes in IMMORTALITY
* changes in SUICIDE VEST

### 28b

* remove behaviors from SOLAR FLAME and INCENDIATOR
* add new weapon: SUICIDE VEST (with behavior)
* add new weapon: INVULNERABILITY (with behavior)
* add new weapon: SILENT KILLER (with behavior)
* add new weapon: IMMORTALITY (with behavior)

### 28a

* add behavior to SOLAR FLAME and INCENDIATOR (leaves burning flames sticking to worms)
* add behavior to KEVLAR MISSILE (destroyable + shooting at enemy worms)
* add behavior to DARTGUN (bullets stick to worms)
* add behavior to VEHICLE (destroyable)
* small change in BARRIER (now bullets are not affected by barrier)
* add more behaviors (unused)