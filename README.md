## webliero extended mods

these mods use extended features from Webliero Extended see below

all "mods" prefixed by "_" (example: "_lava") is meant to be a "template" to be merged into another mod using [https://www.vgm-quiz.com/dev/webliero/wledit/](WLedit)

PNG files are deployed directly except if they're in a {$author}/01_anim/{$animation_name} => *these will be deployed as zip files containing all the pngs*

### REQUIRED!

you need to use the extended client javascript to connect to rooms running these modifications
follow the steps here for how to:
[https://bit.ly/lieroX](https://bit.ly/lieroX)

room script by dsds most hacking by Ophi

## PAGE URL

you'll find all the files deployed here:

[https://sylvodsds.gitlab.io/webliero-extended-mods/](https://sylvodsds.gitlab.io/webliero-extended-mods/)

you can use these links in the room script
